<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Register</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <!-- START NAVIGATION BAR-->
    <div class="row">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input class="form-control" placeholder="Search" type="text">
                </div> 
                <button class="btn btn-default" type="submit">Submit</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

    </div>
    <!-- END NAVIGATION BAR-->
    <div class="row">
        <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1>Registration</h1>
                    <p><?php echo lang('create_user_subheading');?></p>
                </div>
                <div class="panel-body">
<?php if(!empty($message)): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $message;?>
    </div>
<?php endif; ?>

    <?php echo form_open("auth/create_user");?>
<div class="col-md-6">
    <div class="form-group">
        <?php echo lang('create_user_fname_label', 'first_name');?> <br />
        <?php echo form_input($first_name);?>
    </div>
    <div class="form-group">
        <?php echo lang('create_user_company_label', 'company');?> <br />
        <?php echo form_input($company);?>
    </div> 
</div>
<div class="col-md-6">
    <div class="form-group">
        <?php echo lang('create_user_lname_label', 'last_name');?> <br />
        <?php echo form_input($last_name);?>
    </div> 
    <div class="form-group">
        <?php echo lang('create_user_email_label', 'email');?> <br />
        <?php echo form_input($email);?>
    </div> 
    <div class="form-group">
        <?php echo lang('create_user_phone_label', 'phone');?> <br />
        <?php echo form_input($phone);?>
    </div> 
</div>
<div class="col-md-6">
    <div class="form-group">
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
    </div> 
</div>

<div class="col-md-6">
    <div class="form-group">
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
       <input type="submit" class="btn btn-primary form-control" value="Register" />
    </div>  
</div>
    <?php echo form_close();?>      

                </div>
            </div>
        </div>
        </div>

<!--p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p-->

       
    </div>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>
