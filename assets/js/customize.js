/*
 * JS addons
 */
 
 /*
  * Simple Timer, Fully Customize
  */
var CountDown = (function ($) {
    // Length ms 
    var TimeOut = 10000;
    // Interval ms
    var TimeGap = 1000;
    
    var CurrentTime = ( new Date() ).getTime();
    var EndTime = ( new Date() ).getTime() + TimeOut;
    
    var GuiTimer = $('#countdown');
    //var GuiPause = $('#pause');
    //var GuiResume = $('#resume').hide();
    
    var Running = true;
    
    var UpdateTimer = function() {
        // Run till timeout
        if( CurrentTime + TimeGap < EndTime ) {
            setTimeout( UpdateTimer, TimeGap );
        }
        // Countdown if running
        if( Running ) {
            CurrentTime += TimeGap;
            if( CurrentTime >= EndTime ) {
                /*GuiTimer.css('color','red');*/
                //alert('timer expire');
                return;
            }
        }
        // Update Gui
        var Time = new Date();
			Time.setTime( EndTime - CurrentTime );
        
		var Days    = (EndTime - CurrentTime) / (60*60*24*1000);
		var Hours   = ((EndTime - CurrentTime) / (60*60*1000) >= 24) ? ((EndTime - CurrentTime) / (60*60*1000))-24 : (EndTime - CurrentTime) / (60*60*1000);
		var Minutes = Time.getMinutes();
        var Seconds = Time.getSeconds();
        
		
		// Display TIME
		var display_clock = '<span class="fa fa-clock-o"></span> &nbsp;&nbsp;';
			if(Math.floor(Days) > 0) {
				display_clock += '<strong>' + Math.floor(Days) + '</strong>d <strong>' + Math.floor(Hours) + '</strong>hr <strong>' + Minutes + '</strong>min';
			}
			else {
				display_clock += '<strong>' + Math.floor(Hours) + '</strong>hr <strong>' + Minutes + '</strong>min <strong>' + Seconds + '</strong>sec';
			}
		//console.log((EndTime - CurrentTime) / (60*60*1000));
		
		GuiTimer.html( display_clock );
    };
    
    var Pause = function() {
        Running = false;
        GuiPause.hide();
        GuiResume.show();
    };
    
    var Resume = function() {
        Running = true;
        GuiPause.show();
        GuiResume.hide();
    };
    
    var Start = function( Timeout ) {
        /*TimeOut = Timeout;
        CurrentTime = ( new Date() ).getTime();
		EndTime = ( new Date() ).getTime() + TimeOut;*/
		
		CurrentTime = ( new Date() ).getTime();
		EndTime = Timeout.getTime();
		
		//console.log(new Date());
		//console.log(Timeout);
        
		UpdateTimer();
    };

    return {
        Pause: Pause,
        Resume: Resume,
        Start: Start
    };
})(jQuery);
//console.log(new Date());

$(document).ready(function(){
	
	$('.close').alert('close');
	//Calculation
	var calculate_percent_value = function(val, percent, element) {
		var form = val.closest('form');
		var value = parseInt(val.val()) * percent;
			value = isNaN(value) ? '00.00' : value;
			form.find(element).text(parseInt(value).toFixed(2));
			
			return parseInt(value);
	}
	
	//Change Onload
	function onload_calculate() {
		var current_value = $('.current-bid').text();
		$('#tax-value').text( ((parseFloat(current_value.substring(1)))*0.05).toFixed(2) );
		$('#shipping-value').text( ((parseFloat(current_value.substring(1)))*0.07).toFixed(2) );
		$('#total-value').text(  (parseFloat(current_value.substring(1)) + (parseFloat(current_value.substring(1)))*0.05 + (parseFloat(current_value.substring(1)))*0.07).toFixed(2) );
	}
	onload_calculate();
	
	//For numeric only
	$('#bid-value').on('keypress', function(event){
		if (event.keyCode < 48 || event.keyCode > 57)
			return false;
	});
	
	//Submit Bidding 
	function bidSubmit()
	{
	$('form.form-bid').submit(function(e){
		e.preventDefault();
		this_form = $(this);
		
		var product_id = parseInt($(this).attr('product_id'))
		var ajax_url = '/index.php/auth/product_detail/'+product_id;
		var data = $(this).serialize();
		$.post(ajax_url, data, function(response){
			console.log(response);
			if(response != 'true') {
				this_form.find('.response').before('<div class="form-group alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-close"></span></button>'+response+'</div>');
			}
			else {
				$('#product-reload').load(ajax_url + ' #product-reload > *', function(){
					onload_calculate();
					bidSubmit();
				});
				//console.log(response);
			}
		});
			
	});
	}
	bidSubmit();

});



