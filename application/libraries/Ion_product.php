<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Product
*
*/

class Ion_product
{
	#protected $status;

	
	public function __construct()
	{
		$this->load->library(array('email'));
		$this->load->library('session');
		$this->load->helper(array('cookie', 'language','url'));
		
		$this->load->model('ion_product_model');
	}

	
	public function __call($method, $arguments)
	{
		if ( !method_exists($this->ion_product_model, $method) )
		{
			throw new Exception('Undefined method Ion_auth::' . $method . '() called');
		}
		return call_user_func_array( array($this->ion_product_model, $method), $arguments);
	}


	/*
	 * Get the list of Products: filter by parameter
	 */
	public function get_products($params = false)
	{
		if(!empty($params)):
			//Filter data for database
			if($params['status']):
				$arr_status = array('all','sold', 'unsold', 'ongoing');
				$params['status'] = (in_array($params['status'], $arr_status)) ? $params['status'] : 'all';
			endif;

			$arr_order_by = array('highest_bid', 'highest_bidders');
			$params['order_by'] = (in_array($params['order_by'], $arr_order_by)) ? $params['order_by'] : false;
		endif;

		$results =  $this->ion_product_model->get_products_raw($params)->result();
		return $results;
	}

	/*
	 * Get product detail information
	 */
	public function get_product_detail($id)
	{
		$results =  $this->ion_product_model->get_product_detail_raw($id)->row();
		return $results;
	}

	/*
	 */
	/*
	 * Get list of user bidding per product
	 */
	public function get_users_bid($id) 
	{
		$results =  $this->ion_product_model->get_users_bid_raw($id)->result();
		return $results;
	}	

	/*
	 * Get bidder information: param $id =  user id
	 */

	public function get_bidder_information($id) {
		$results = $this->ion_product_model->get_bidder_information_raw($id)->row();
		return $results;
	}

	/*
	 * Get Product Categories
	 */
	public function get_product_categories()
	{
		$results = $this->ion_product_model->get_product_categories_raw()->result();
		return $results;
	}

	/*
	 * Get product detail information
	 */

	public function insert_user_bid($product_id, $user_id, $bid_price)
	{
		//check bid is present must return true or false
		if($this->ion_product_model->check_user_bid($product_id, $user_id)):
			//update if present
			$this->ion_product_model->update_user_bid($product_id, $user_id, $bid_price);
		else:
			//insert if not in
			$this->ion_product_model->insert_user_bid($product_id, $user_id, $bid_price);
		endif;
		return true;
	}

	/*
	 * Insert Product By Batch: $data array of data
	 */
	public function insert_product_csv($data)
	{
		$filtered_data = array();
		
		//filter data to inserted into database;
		if(!empty($data)):
		foreach($data as $k_d => $v_d):
			$filtered_data[$k_d] = mysql_real_escape_string($v_d);
			if($k_d == 'price')
				$filtered_data[$k_d] = intval($filtered_data[$k_d]);
			elseif($k_d == 'bid_start' OR $k_d == 'bid_end')
				$filtered_data[$k_d] = date('Y-m-d H:i:s', strtotime($filtered_data[$k_d]));
		endforeach;
		endif;	

		$insert = $this->ion_product_model->insert_product_csv($filtered_data);

		if(!is_int($insert) OR $insert < 0)
			return false;

		return $this->get_product_detail($insert);
	}
	/*
	 * Save new product, $data: table column -> column value; attr = optional
	 */

	public function save_product($data, $data_attr = false)
	{
		return $this->ion_product_model->save_product_raw($data, $data_attr);
	}


	/*
	 * Search Array keys and values
	 */
	public function search_data($key, $value, $array)
	{
		$data = array();
		if(!empty($array)):
			foreach($array as $k => $v):
				if($v->$key == $value)
					$data = $v;
			endforeach;	
		endif;
		return $data;
	}


	/*
	 *Very Important function: reference for $this - I donot understand why please research :D
	 */
	public function __get($var)
	{
		return get_instance()->$var;
	}
}
