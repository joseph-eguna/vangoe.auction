<?php 
    /*
     * Filter the data 
     */

    date_default_timezone_set('America/Los_Angeles');
    
    $time_bidstart = strtotime($product->bid_start);
    $time_bidend   = strtotime($product->bid_end);
    $time_current   = strtotime(date('Y:m:d H:i:s'));

    #var_dump($time_bidstart - $time_current); echo '<br />';
    #var_dump(strtotime($product->bid_end)); echo '<br />';
    #var_dump(date('Y:m:d H:i:s')); echo '<br />';

    #Calculation for current bid value
    $current_bid = intval($product->price);
    if(!empty($product_bidders)):
        $bid_winner = $product_bidders[0]->user_id;
        //Bid winner information
        $bid_winner_info = $this->ion_product->get_bidder_information($bid_winner);

        if(intval($product_bidders[1]->bid_price) > 0):
            $current_bid = intval($product_bidders[1]->bid_price) + ((intval($product_bidders[0]->bid_price) - intval($product_bidders[1]->bid_price)) / 2);
        else:
            $current_bid = $current_bid + ((intval($product_bidders[0]->bid_price) - $current_bid)/2);
        endif;

        if($bid_winner == $this->session->userdata('user_id'))
            $current_bid = intval($product_bidders[0]->bid_price);
    endif;

    //Minimum bid allowed
    $bid_note_value = $current_bid + intval($current_bid*0.02);
?>

<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$product->name?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- START NAVIGATION BAR-->
        <?=$top_nav?>
    <!-- END NAVIGATION BAR-->
    <div class="row">
        <div class="container">
            <!-- SIDEBAR -->
            <div class="col-sm-3 col-lg-3 col-md-3">
                <?=$leftside_nav ?>
            </div>
            <!-- END SIDEBAR -->
        	
            <!-- CONTENT-->
            <div class="col-sm-9 col-lg-9 col-md-9">
                <div class="col-md-12">
                <?php if(($time_bidstart-$time_current) < 0): ?>
                    <div id="countdown" class="pull-right"></div>
                <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-9 col-lg-9 col-md-9" id="product-reload">
                <!-- response message here -->
        <?php if(!empty($product_user_check)): ?>        
                <div class="col-md-12">
            <?php if($bid_winner != $this->session->userdata('user_id')):?>
                <div class="alert alert-danger" role="alert">
                    <p>
                        Sorry, You are <strong>Out bidden</strong> by the others.
                    </p>
                </div>
                
            <?php else: ?>
                <div class="alert alert-success" role="alert">
                    
                    <p>
                       <strong>Congratulations! </strong> You are the highest bidder.
                    </p>
                </div>
            <?php endif; ?>
                </div>
        <?php endif; ?>
            <!-- end response message -->
            <div class="col-sm-6 col-lg-6 col-md-6">
                <div class="thumbnail">
                    <img src="<?=$product->image_url; ?>" alt="">
                        <div class="caption">
                            <h4 class="pull-right">$<?=$product->price?></h4>
                            <h4><a href="#"><?=$product->name; ?></a></h4>
                                <p><?=$product->description; ?></p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">15 reviews</p>
                            <p>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                            </p>
                        </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-6 col-md-6">
                <div class="panel panel-info bid-panel">
                    <div class="panel-heading">
                        <h6>CURRENT BID: </h6>
                        <h3 class="current-bid">$<?=$current_bid?></h3>
                    </div>
                    <div class="panel-body">

<?php if(($time_bidstart-$time_current) > 0): ?>
<div class="form-group col-md-12" style="text-align: center;">
    Auction will start at: <br>
    <label><?=date('D M j g:i a T Y', strtotime($product->bid_start)); ?></label><br>  
    And will end at: <br>
    <label><?=date('D M j g:i a T Y', strtotime($product->bid_end)); ?> </label>
</div>
<?php elseif(($time_bidend-$time_current) < 0): ?>
<div class="row">
<!-- START STATEMENT -->  
    <?php if(!empty($product_bidders)):?>
    <div class="col-md-12 text-success">
        <h4><span class="fa fa-star"></span> Bid Winner</h4>
    </div>
        <img alt="Bid Winner" class="col-md-4 img-circle" src="/assets/images/auction-avatar.jpg" style="" />
    <div class="caption col-md-8">
        <label class="author"><?php echo $bid_winner_info->first_name.' '.$bid_winner_info->last_name ?></label><br>

        <p class="text-muted">Lorem ipsum dolor sit amet, consectetuer
        adipiscing elit. Aenean commodo. 
        <a href="#"><span class="fa fa-facebook"></span></a> 
        <a href="#"><span class="fa fa-twitter"></span></a></p>
    </div>
    <?php else: ?>
    <div class="col-md-12">
    <div class="alert alert-warning" role="alert">
        <span class="fa fa-exclamation-circle"></span> 
           This product was not schedule yet!
    </div>
    <a href="#" class="btn btn-primary" id="notify_me">Notify me!</a>
    </div>
    
    <?php endif; ?>
<!-- END STATEMENT -->
</div>
<?php else: ?>
<form class="form-horizontal col-md-12 form-bid" method="POST" product_id="<?=$product->ID?>">
    <input type="hidden" name="minimum_bid" value="<?=$bid_note_value;?>" />
    <div class="form-group response">
        <label>Enter your bid:</label>
        <div class="input-group">
            <div class="input-group-addon">$</div>
            <input class="form-control" id="bid-value" name="bid_amount" placeholder="Amount" type="text">
            <div class="input-group-addon">.00</div>
        </div>
        <strong class="text-muted" style="text-align: center;font-size: 13px;display: block;">
            (You must bid <?=$bid_note_value;?> or higher)
        </strong>
    </div>
    <div class="form-group">
<table class="table table-bordered">
    <tbody>
        <tr>
            <td>Tax:</td>
            <td id="tax-value">00.00</td>
        </tr>
        <tr>
            <td>Shipping:</td>
            <td id="shipping-value">00.00</td>
        </tr>
        <tr class="success">
            <td>Total Payment:</td>
            <td id="total-value">00.00</td>
        </tr>
    </tbody>
</table>
    </div>
    <div class="form-group">
        <button class="btn btn-danger form-control" type="submit"><strong>PLACE BID</strong></button>
    </div>
</form>  
<?php endif;  ?>             

                    </div>
                </div>
            </div>
            </div>
            <!-- END CONTENT -->
	</div>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/customize.js"></script>
    
    <script type="text/javascript">
        //May 31, 2015 1:25:00
        CountDown.Start(new Date("<?=date('M d, Y H:i:s', strtotime($product->bid_end)); ?>"));
    </script>

</body>
</html>