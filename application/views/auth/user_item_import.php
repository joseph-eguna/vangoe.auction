<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Import Collection Items</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <?=$top_nav?>
    <div class="row">
        <div class="container-fluid">
            <!-- SIDEBAR -->
            <div class="col-sm-2 col-lg-2 col-md-2">
            <?=$leftside_nav?>
            </div>
            <!-- END SIDEBAR -->
        	
            <!-- CONTENT-->
            <div class="col-sm-10 col-lg-10 col-md-10">


<div class="panel panel-primary">
    <div class="panel-heading">
        <div>
            <h1 class="col-md-9" style="margin-top:0;">Import from CSV File</h1>
            <a href="/assets/example-csv-format.csv" title="Export Example CSV format" class="col-md-3 btn btn-default pull-right">CSV Format</a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="panel-body">
        <?php if(!empty($message)): ?>
            <?php if(is_array($message)): ?>
            <?php foreach($message as $mes): ?>
                <div class="alert alert-danger" role="alert"><?=$mes?></div>
            <?php endforeach; ?>
            <?php else: ?>
                <div class="alert alert-success" role="alert"><?=$message?></div>
            <?php endif; ?>
        <?php endif; ?>

        <form class="form-horizontal save_product" method="POST" enctype="multipart/form-data">
            <div class="col-md-8">
                <div class="form-group">
                   <?php echo form_input($file_import);?>
                    <label>Select CSV File:</label>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">    
                <div class="form-group">
                    <input type="hidden" name="nounce" value="form-import-items">
                    <input class="btn btn-primary form-control" name="submit" type="submit" value="IMPORT NOW">
                </div>
            </div>
        </form>

        <?php if(!empty($csv_data)): ?>
        <table class="table table-bordered table-hover">
            <tbody>
            <tr class="info">
                <td colspan="6">Newly Added Items</td>
            </tr>
            </tbody>
            <tbody>
            <tr class="text-center">
                <td>Price</td>
                <td>Image</td>
                <td>Name</td>
                <td>Description</td>
                <td>Auction Start</td>
                <td>Auction End</td>
            </tr>
            </tbody>
            <?php foreach($csv_data as $csv_d): ?>
            <tbody>
            <tr>
                <td><h3 class="text-danger"><?=$csv_d->price?></h3></td>
                <td><img src="<?=$csv_d->image_url?>" class="img-responsive" style="width:200px;" title="<?=$csv_d->name?>" alt="<?=$csv_d->name?>"/></td>
                <td><h4><?=$csv_d->name?></h4></td>
                <td class="text-muted"><?=substr($csv_d->description, 0, 100)?>[...]</td>
                <td><strong><?=date('M d, Y h:i a', strtotime($csv_d->bid_start)); ?></strong></td>
                <td><strong><?=date('M d, Y h:i a', strtotime($csv_d->bid_end)); ?></strong></td>
            </tr>
            </tbody>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

            </div>
            <!-- END CONTENT -->

		</div>
	</div>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.ui.js"></script>
    <script type="text/javascript">
        $( ".datepicker" ).datepicker();
    </script>
</body>
</html>