<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>My Collection</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <?=$topnav?>
    <div class="row">
        <div class="container-fluid">
            <!-- SIDEBAR -->
            <div class="col-sm-2 col-lg-2 col-md-2">
            <?=$leftside_nav?>
            </div>
            <!-- END SIDEBAR -->
        	
            <!-- CONTENT-->
            <div class="col-sm-10 col-lg-10 col-md-10">

<form class="form-inline" method="POST">
    <div class="form-group">
        <label>Filter By:</label> 
        <select class="form-control" name="status" onchange="this.form.submit()">
            <option value="all" <?php echo ($filter_status == 'all') ? 'selected' : ''; ?>>All Items</option>
            <option value="sold" <?php echo ($filter_status == 'sold') ? 'selected' : ''; ?>>Sold Items</option>
            <option value="unsold" <?php echo ($filter_status == 'unsold') ? 'selected' : ''; ?>>Unsold Items</option>
            <option value="ongoing" <?php echo ($filter_status == 'ongoing') ? 'selected' : ''; ?>>On Auction Items</option>
        </select>
    </div>
    &nbsp;&nbsp;
    <div class="form-group">
        <label>Sort By:</label> 
        <select class="form-control" name="order_by" onchange="this.form.submit()">
            <option value="latest_added" <?php echo ($filter_orderby == 'latest_added') ? 'selected' : ''; ?>>Latest Added</option>
            <option value="highest_bid" <?php echo ($filter_orderby == 'highest_bid') ? 'selected' : ''; ?>>Highest Bid</option>
            <option value="highest_bidders" <?php echo ($filter_orderby == 'highest_bidders') ? 'selected' : ''; ?>>Hot List</option>
        </select>
    </div>

    <a href="/index.php/auth/user_item_add"  title="Add New Item" class="btn btn-primary pull-right">Add New</a>
    <a href="/index.php/auth/user_item_import"  style="margin-right:5px;"  title="Import Items" class="btn btn-primary pull-right">Import Items</a> &nbsp;
</form> 
 <div class="list-group">
<?php if(!empty($products)): ?>
    <?php foreach($products as $product): ?>
    <div class="list-group-item col-sm-12 col-lg-12 col-md-12">
        <div class="col-sm-2 col-lg-2 col-md-2">
            <img alt="" class="pull-left" src="<?=$product->image_url;?>" style="max-width: 100%; margin-bottom: 10px;">
            <h3 class="text-danger text-center">$<?=($product->highest_bid) ? $product->highest_bid : $product->price; ?></h3>
            <span class="text-muted text-center" style="font-size: 11px; text-transform: uppercase; margin-top: -8px; text-align: center; display: block;">
            (Highest Bid)</span>
            <p>
                <span class="text-muted text-center" style="font-size: 11px; text-transform: uppercase; margin-top: -8px; text-align: center; display: block;"></span>
            </p>
        </div>
        <div class="col-sm-10 col-lg-10 col-md-10">
            <h4>
                <a href="/index.php/auth/product_detail/<?=$product->ID?>">
                    <?php echo $product->name; ?>
                </a>
            </h4>
            <p><?=substr($product->description, 0, 200);?></p>
            <h5 style="display:inline-block;margin-right: 10px;">
            <span class="fa fa-users"></span> <?=$product->highest_bidders;?> bidders</h5>
            <h5 style="display:inline-block;margin-right:10px;">
                <span class="fa fa-calendar"></span> 
                <?=date("M d, Y h:i a", strtotime($product->bid_start)); ?> to <?=date("M d, Y h:i a", strtotime($product->bid_end)); ?> 
            </h5>
            <h5 style="display:inline-block;margin-right:10px;">
                <span class="fa fa-exclamation-triangle"></span> 
                <?php if(strtotime($product->bid_start) < strtotime('now')
                        AND strtotime($product->bid_end) > strtotime('now')): ?>
                    <strong class="text-danger">ONGOING AUCTION</strong>
                <?php elseif(strtotime($product->bid_start) > strtotime('now')): ?>
                    <strong class="text-danger">ON AUCTION SOON</strong>
                <?php elseif(strtotime($product->bid_start)  < strtotime('now') 
                        AND !empty($product->bidders)): ?>  
                    <strong class="text-danger">SOLD</strong>  
                <?php elseif(strtotime($product->bid_start)  < strtotime('now')): ?>
                    <strong class="text-danger">UNSOLD</strong>
                <?php endif; ?>
            </h5>
        </div>
    </div>
    <?php endforeach; ?>
<?php endif; ?>
</div>
<nav>
    <ul class="pagination pull-right">
        <li><a href="#"><span>«</span></a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#"><span>»</span></a></li>
    </ul>
</nav>

            </div>
            <!-- END CONTENT -->

		</div>
	</div>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>