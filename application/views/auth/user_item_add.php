<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add New Collection</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <!-- START NAVIGATION BAR-->
    <div class="row">
<nav class="navbar navbar-default" style="">
    <div class="container-fluid">
        <div class="collapse navbar-collapse">
            <div class="container">
                <form class="navbar-form navbar-left">
                    <div class="form-group">
                        <input class="form-control" placeholder="Search" type="text">
                    </div>
                    <button class="btn btn-default" type="submit">Submit</button>
                </form>

                <ul class="nav navbar-nav pull-right" style="font-size:25px;">
                    <li>
                        <a href="/index.php/auth/user_profile" title="My Account"><span class="fa fa-user"></span></a>
                    </li>
                    <li>
                        <a href="/index.php/auth/logout" title="Logout"><span class="fa fa-sign-out"></span></a>
                    </li>
                </ul>
            </div>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
    </div>
    <!-- END NAVIGATION BAR-->
    <div class="row">
        <div class="container">
            <!-- SIDEBAR -->
            <div class="col-sm-3 col-lg-3 col-md-3">
            <?=$leftside_nav?>
            </div>
            <!-- END SIDEBAR -->
        	
            <!-- CONTENT-->
            <div class="col-sm-9 col-lg-9 col-md-9">


<div class="panel panel-primary">
    <div class="panel-heading">
        <div>
            <h1 class="col-md-12">
                Add New Collection
            </h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="panel-body">
        <form class="form-horizontal save_product" method="POST" enctype="multipart/form-data">
            <div class="col-md-12"><?=$message;?></div>
            <div class="col-md-6">
                <div class="form-group col-md-12">
                    <label>Collection Name:</label>
                    <?php echo form_input($product_name);?>
                </div>
                <div class="form-group col-md-12">
                    <label>Collection Price:</label> 
                    <?php echo form_input($product_price);?>
                    <p class="text-muted">
                        (Enter the minimum collection price.)
                    </p>
                </div>
                <div class="form-group col-md-12">
                    <label>Collection Image:</label> 
                    <?php echo form_input($product_image);?>
                    <p class="text-muted">(Enter Image URL.)</p>
                </div>
                <div class="form-group col-md-12">
                    <label>Collection Category:</label> 
                    <select multiple="multiple" class="form-control" name="product_category[]">
                        <?php if(!empty($categories)): foreach($categories as $category): ?>
                            <option value="<?=$category->ID?>"><?=$category->name?></option>
                        <?php endforeach; endif; ?>
                     </select>
                </div>
            </div>
            <div class="col-md-6">
                
                <div class="form-group col-md-12">
                    <label>Auction Start:</label> 
                    <input class="form-control datepicker" name="product_auction_start" type="text">
                    <p class="text-muted">
                        (Enter when will auction start.)
                    </p>
                </div>
                <div class="form-group col-md-12">
                    <label>Auction End:</label> 
                    <input class="form-control datepicker" name="product_auction_end" type="text" />
                    <p class="text-muted">
                        (Enter when will auction end.)
                    </p>
                </div>

                <div class="form-group col-md-12">
                    <label>Collection Short Description:</label> 
                    <?php echo form_textarea($product_short_description);?>
                </div>
                <div class="form-group col-md-12">
                    <label>Collection Full Description:</label> 
                    <?php echo form_textarea($product_full_description);?>
                </div>
                <div class="form-group col-md-12">
                    <input type="hidden" name="nounce" value="form-add-new">
                    <input class="btn btn-primary form-control" name="submit" type="submit" value="PUBLISH NOW">
                </div>
            </div>
        </form>
    </div>
</div>

            </div>
            <!-- END CONTENT -->

		</div>
	</div>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.ui.js"></script>
    <script type="text/javascript">
        $( ".datepicker" ).datepicker();
    </script>
</body>
</html>