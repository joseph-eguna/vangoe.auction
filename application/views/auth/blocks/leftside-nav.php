            <div class="list-group">
                <?php if(!empty($nav_menu_left)): ?>
                    <?php foreach($nav_menu_left as $nav):?>
                    <?php $nav['active'] = isset($nav['active']) ? 'active' : ''; ?>
                    <a href="<?=$nav['link']?>" class="list-group-item <?=$nav['active']?>"><?=$nav['title']?></a>
                    <?php endforeach; ?>
                <?php endif; ?>
                
            </div>