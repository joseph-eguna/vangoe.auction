<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <!-- START NAVIGATION BAR-->
    <div class="row">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input class="form-control" placeholder="Search" type="text">
                </div> 
                <button class="btn btn-default" type="submit">Submit</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

    </div>
    <!-- END NAVIGATION BAR-->
    <div class="row">
        <div class="container">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                     <h1><?php echo lang('login_heading');?></h1>
                    <p><?php echo lang('login_subheading');?></p>
                </div>
                <div class="panel-body">
<?php if(!empty($message)): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $message;?>
    </div>
<?php endif; ?>
    <?php $id = !isset($id) ? '' : $id;?>
    <?php echo form_open("auth/login/{$id}");?>
    <div class="form-group">
        <?php echo lang('login_identity_label', 'identity');?>
        <?php echo form_input($identity); ?>
    </div>  
    <div class="form-group">
       <?php echo lang('login_password_label', 'password');?>
       <?php echo form_input($password);?>
    </div> 
    <div class="form-group">
       <?php echo lang('login_remember_label', 'remember');?>
       <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
    </div> 
    <div class="form-group">
       <input type="submit" class="btn btn-primary form-control" value="Login" />
    </div>   
    <?php echo form_close();?>      

                </div>
            </div>
        </div>
        </div>

<!--p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p-->

       
    </div>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>