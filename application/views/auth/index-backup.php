<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="http://api.system.local/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://api.system.local/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="http://api.system.local/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <!-- START NAVIGATION BAR-->
    <div class="row">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input class="form-control" placeholder="Search" type="text">
                </div> 
                <button class="btn btn-default" type="submit">Submit</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
        		<li><a href="/index.php/auth/logout" title="Logout"><span class="fa fa-sign-out"></span>Logout</a></li>
      		</ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

    </div>
    <!-- END NAVIGATION BAR-->
    <div class="row">
        <div class="container">
<h1><?php echo lang('index_heading');?></h1>
<p><?php echo lang('index_subheading');?></p> 

<div id="infoMessage"><?php echo $message;?></div>
<table class="table table-bordered table-hover">
	<tr>
		<th><?php echo lang('index_fname_th');?></th>
		<th><?php echo lang('index_lname_th');?></th>
		<th><?php echo lang('index_email_th');?></th>
		<th><?php echo lang('index_groups_th');?></th>
		<th><?php echo lang('index_status_th');?></th>
		<th><?php echo lang('index_action_th');?></th>
	</tr>
	<?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td><!--
				<?php foreach ($user->groups as $group):?>
					<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                <?php endforeach?>-->
			</td>
			<td><!--<?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?>--></td>
			<td><!--<?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?>--></td>
		</tr>
	<?php endforeach;?>
</table>
<!--p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p-->

        </div>
	</div>
    <script src="http://api.system.local/assets/js/jquery.js"></script>
    <script src="http://api.system.local/assets/js/bootstrap.min.js"></script>
</body>
</html>