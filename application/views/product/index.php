<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Browse Collectors Items</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <!-- START NAVIGATION BAR-->
        <?=$topnav?>
    <!-- END NAVIGATION BAR-->
    <div class="row">
        <div class="container">
            <!-- CONTENT-->
        <?php if(!empty($products_latest)):  
                foreach($products_latest as $product):
        ?>
            <div class="col-sm-4 col-lg-3 col-md-3">
                <div class="thumbnail">
                    <img src="<?=$product->image_url; ?>" alt="">
                        <div class="caption">
                            <h4 class="pull-right">$<?=$product->price?></h4>
                            <h4><a href="/index.php/auth/product_detail/<?=$product->ID?>"><?=$product->name; ?></a></h4>
                                <p><?=substr($product->description, 0, 100).'...'?></p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right"><?php echo count($this->ion_product->get_users_bid($product->ID)); ?> bidders</p>
                            <p>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                            </p>
                        </div>
                </div>
            </div>
        <?php endforeach; endif; ?>
            <!-- END CONTENT -->

        </div>
    </div>
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
</body>
</html>