<!-- START NAVIGATION BAR-->
    <div class="row">
<nav class="navbar navbar-default" style="">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id=
        "bs-example-navbar-collapse-1">
            <div class="container">
                <form class="navbar-form navbar-left">
                    <div class="form-group">
                        <input class="form-control" placeholder="Search" type="text">
                    </div>
                    <button class="btn btn-default" type="submit">Submit</button>
                </form>

                <ul class="nav navbar-nav pull-right">
                    <li>
                        <a href="/index.php/auth/logout" title="Logout"><span class="fa fa-sign-out"></span></a>
                    </li>
                </ul>
            </div>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

    </div>
    <!-- END NAVIGATION BAR-->