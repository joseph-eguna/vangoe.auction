<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="http://api.system.local/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://api.system.local/assets/css/custom.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="http://api.system.local/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>
    <div class="row">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <h1><?php echo lang('login_heading');?></h1>
                    <p><?php echo lang('login_subheading');?></p>
                </div>
                <div class="panel-body">
    <div id="infoMessage"><?php echo $message;?></div>

    <?php echo form_open("auth/login");?>
    <div class="form-group">
        <?php echo lang('login_identity_label', 'identity');?>
        <?php echo form_input($identity);?>
    </div>  
    <div class="form-group">
       <?php echo lang('login_password_label', 'password');?>
       <?php echo form_input($password);?>
    </div> 
    <div class="form-group">
       <?php echo lang('login_remember_label', 'remember');?>
       <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
    </div> 
    <div class="form-group">
       <?php echo form_submit('submit', lang('login_submit_btn'));?>
    </div>   
    <?php echo form_close();?>      

                </div>
            </div>
        </div>

<!--p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p-->

       
    </div>
    <script src="http://api.system.local/assets/js/jquery.js"></script>
    <script src="http://api.system.local/assets/js/bootstrap.min.js"></script>
</body>
</html>