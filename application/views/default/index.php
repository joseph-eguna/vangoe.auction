<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Auction Website</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="http://api.system.local/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://api.system.local/assets/css/custom.css" rel="stylesheet">
	<link href="http://api.system.local/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-topx navbar-shrink">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top" style="text-transform: none;font-weight: 100;font-family: arial;">Its my pleasure to help you.</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden active">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Downloads</a>
                    </li>
                    
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--img class="img-responsive" style="  width: 200px;" src="http://api.system.local/assets/img/profile.png" alt="" />
                    <!--div class="intro-text">
                        <span class="skills">Download the preview of awesome bootstrap template for Free? Yes it is for real. I am Joseph and collect list of awesome bootstrap template just for you. Imagine that. <span class="fa fa-smile-o"></span></span>
                    </div-->
                </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
			<?php for( $a=0; $a<6; $a++ ): ?>
                <div class="col-sm-4" style="margin-bottom:30px;">
					<img src="http://api.system.local/assets/img/portfolio/cabin.png" class="img-responsive" alt="" />
                </div>
			<?php endfor; ?>	
            </div>
        </div>
    </section>
	<!-- About Section -->
    

    <!-- Contact Section -->
    

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright © Your Website 2015
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
	<!-- jQuery -->
    <script src="http://api.system.local/assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="http://api.system.local/assets/js/bootstrap.min.js"></script>
</body>
</html>