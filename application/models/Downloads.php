<?php 
class Downloads extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //Get list of downloadable files
    public function get_downloadables(){
    	$sql = "SELECT * FROM api_downloads";
    	$result = $this->db->query($sql);
    	return $result->result_array();
    }

    //Check specific file to down
    public function check_download($hash = false){
    	if(!$hash)
    		return false;

    	$sql = "SELECT * FROM api_downloads WHERE hash = '".$hash."'";
    	$result = $this->db->query($sql);

    	if(empty($result->row()))
    		return false;

    	return $result;
    	
	}
}