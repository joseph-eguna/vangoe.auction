<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Model
*
* Version: 2.5.2
*
* Author:  Ben Edmunds
* 		   ben.edmunds@gmail.com
*	  	   @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Last Change: 3.22.13
*
* Changelog:
* * 3-22-13 - Additional entropy added - 52aa456eef8b60ad6754b31fbdcc77bb
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

class Ion_product_model extends CI_Model
{
	#public $tables = array();
	var $error = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	/*
	 * Get the list of products here, a lot more filter soon
	 */
	public function get_products_raw($params = false) 
	{

		$params['limit'] 	= (!isset($params['limit'])) ? 10 : $params['limit'];
		$params['cat_id'] 	= (!isset($params['cat_id'])) ? false : $params['cat_id'];
		$params['owner_id'] = (!isset($params['owner_id'])) ? false : $params['owner_id'];
		$params['order_by'] = (!isset($params['order_by'])) ? false : $params['order_by'];
		$params['status'] 	= (!isset($params['status'])) ? false : $params['status'];

/*
SELECT a.*, c.user_id, c.bid_price, MAX(c.bid_price) as highest_bid, count(c.bid_price) as counter
FROM products as a 
LEFT JOIN users_bid as c ON a.ID = c.product_id 
WHERE 1 AND a.owner_id=2 
GROUP BY a.ID
ORDER BY c.bid_price DESC 

 */			 	 

		$sql =  'SELECT a.*, b.category_id, c.user_id, c.bid_price, c.bid_price
				 FROM products as a 
				 LEFT JOIN product_category_relation as b ON a.ID = b.product_id 
				 LEFT JOIN users_bid as c ON b.product_id = c.product_id WHERE 1';	 	 
	

	#var_dump($params);
	if($params['status']):	

		$sql =  'SELECT a.*, MAX(c.bid_price) as highest_bid, count(c.bid_price) as highest_bidders
				 FROM products as a 
			 	 LEFT JOIN users_bid as c ON a.ID = c.product_id 
				 WHERE 1 ';

		#By Status
		if($params['status'] == 'sold'):
			$sql .= ' AND c.bid_price > 0 AND a.bid_end < "'.date('Y-m-d H:i:s', strtotime('now')).'"';
		elseif($params['status'] == 'unsold'):
			$sql .= ' AND c.bid_price is NULL AND a.bid_end < "'.date('Y-m-d H:i:s', strtotime('now')).'"'; //2015-06-06 00:00:00 
		elseif($params['status'] == 'ongoing'):
			$sql .= ' AND a.bid_start < "'.date('Y-m-d H:i:s', strtotime('now')).'" AND a.bid_end > "'.date('Y-m-d H:i:s', strtotime('now')).'"'; 		
		endif;		 

	else:

		#By Category
		if($params['cat_id'])
			$sql .= ' AND b.category_id=' . $params['cat_id']; 	

	endif;

		#By User Items
		if($params['owner_id'])
			$sql .= ' AND a.owner_id=' . $params['owner_id'];
		


		/*$sql .=  ' GROUP BY a.ID
			        ORDER BY c.bid_price DESC'; */		

		$sql .= ' GROUP BY a.ID';
		if(!$params['order_by'])
			$sql .= ' ORDER BY a.date_registered DESC';
		else
			$sql .= ' ORDER BY '.$params['order_by']. ' DESC';

		$sql .= ' LIMIT ' . $params['limit'];	

		#var_dump($sql);  

		$query = $this->db->query($sql);
		return $query;
	}

	/*
	 * Get Product Detail: ID is compulsory
	 */
	public function get_product_detail_raw($id)
	{
		$sql =  "SELECT * FROM products WHERE ID={$id} LIMIT 1";

		$query = $this->db->query($sql);
		return $query;
	}

	/*
	 * Get list or users per product bid
	 */
	public function get_users_bid_raw($id)
	{
		$sql =  "SELECT * FROM users_bid 
				 WHERE product_id={$id}
				 ORDER BY bid_price DESC";
		$query = $this->db->query($sql);
		return $query;
	}

	/*
	 * Get bidder information
	 */
	public function get_bidder_information_raw($id) 
	{
		$sql =  "SELECT * FROM users 
				 WHERE id={$id}
				 LIMIT 1";
		$query = $this->db->query($sql);
		return $query;
	}

	/*
	 * Check user bid
	 */
	public function check_user_bid($product_id, $user_id) {
		
		$sql =  "SELECT * FROM users_bid 
				 WHERE product_id={$product_id}
				 AND   user_id={$user_id} LIMIT 1";

		$query = $this->db->query($sql);

		if($query->row())
			return true;
		return false;
	}

	/*
	 * Get Product categories
	 */
	public function get_product_categories_raw()
	{
		$sql =  "SELECT * FROM product_category 
				 WHERE 1 ORDER BY name ASC";

		$query = $this->db->query($sql);

		return $query;
	}

	/*
	 * Insert user bid
	 */
	public function insert_user_bid($product_id, $user_id, $bid_price) {
		$data = array(
   				'user_id'    => $user_id,
   				'product_id' => $product_id,
  			 	'bid_price'  => $bid_price
			);

		return $this->db->insert('users_bid', $data); 
	}

	/*
	 * Insert product
	 */

	public function insert_product_csv($data) {
		
		$insert_id = 0;
		if($this->db->insert('products', $data))
			$insert_id = $this->db->insert_id();
		else
			$this->error['dberror'] = 'Unable to insert into database';
		
		if(!empty($this->error))
			return $this->error;			

		return $insert_id;
	}

	/*
	 * Insert user bid
	 */
	public function update_user_bid($product_id, $user_id, $bid_price) {
		
		$data = array( 'bid_price' => $bid_price );
		return $this->db->update('users_bid', $data, array('user_id'    => $user_id,
												   		   'product_id' => $product_id));
	}

	
	/*
	 * Save Product Data
	 */

	public function save_product_raw($data, $attr)
	{
		$dbdata = array();

		if(empty($data))
			return false;

		//filter database data
		foreach($data as $kdb => $vdb):
			if($kdb != 'category'):
				$dbdata[$kdb] = $vdb;
			endif;
		endforeach;

		//Insert database
		$this->db->insert('products', $dbdata); 
		$product_id = $this->db->insert_id();
		if(!$product_id)
			$this->error['db_insert'] = 'Unable to Insert data';

		//Insert Product Attribute
		if(!empty($data['category']) OR !$this->db->insert_id()):
			foreach($data['category'] as $cat_id):
				$this->db->insert('product_category_relation', 
									  array('product_id'=>  $product_id,
									  		'category_id'=> $cat_id)); 
			endforeach;
		endif;

		if(!empty($this->error))
			return $this->error;

		return true;
	}
}
