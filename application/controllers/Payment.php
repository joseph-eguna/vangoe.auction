<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	private $template = 'payment/';

	public function __construct(){
		parent::__construct();
		$this->load->library('ChargeBee_Library', '', 'chargebee');
	}
	public function index()
	{
		$this->chargebee->subscription_create();
	}
}
