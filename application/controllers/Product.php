<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'ion_product', 'form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	//redirect if needed, otherwise display the user list
	function index()
	{
		$this->data['products_latest'] = $this->ion_product->get_products();
		
		$this->_render_page('product/index', $this->data);
	}

	/*
	 * Render Page here
	 */
	function _render_page($view, $data=null, $render=false)
	{
		//Top Nav Menu
		$data['topnav'] = $this->load->view('auth/blocks/top-nav', $data, TRUE);
		
		$this->viewdata = (empty($data)) ? $this->data: $data;
		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}

}
