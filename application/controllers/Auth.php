<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	var $data = array();
	var $error = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','ion_product', 'form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		
		//Left Navigation Menu
		$this->data['nav_menu_left'][0]['title'] = 'Browse Items';
		$this->data['nav_menu_left'][0]['link']  = '/index.php/product';

		$this->data['nav_menu_left'][1]['title'] = 'My Collection';
		$this->data['nav_menu_left'][1]['link'] = '/index.php/auth/user_items';

		$this->data['nav_menu_left'][2]['title'] = 'Order History';
		$this->data['nav_menu_left'][2]['link'] = '#';

	}

	//redirect if needed, otherwise display the user list
	function index()
	{

		$this->data['nav_menu_left'][1]['active'] = 'active';
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		/*elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}*/
		else
		{

			$this->data['products_latest'] = $this->ion_product->get_products();
			$this->data['leftside_nav'] = $this->load->view('auth/blocks/leftside-nav', $this->data, TRUE);
			
			$this->_render_page('auth/index', $this->data);
		}
	}


	/*
	 * User list of Items
	 */
	public function user_items(){

		//$this->data['products'] = new stdClass(); //Convert to object

		if (!$this->ion_auth->logged_in())
				redirect('auth', 'refresh');
		
		$this->data['nav_menu_left'][1]['active'] = 'active';
		

		//Form Filter submit
		$this->data['filter_status']  = (!empty($this->input->post('status'))) ? $this->input->post('status') : 'all';
		$this->data['filter_orderby'] = $this->input->post('order_by');


		$this->data['products'] = $this->ion_product->get_products(array( 'limit'=>10,
															'cat_id'=>false,
															'status'=>$this->data['filter_status'],
															'order_by'=>$this->data['filter_orderby'],
															'owner_id'=>$this->session->userdata('user_id')));
		
		/*if(!empty($products)):
			foreach($products as $k_prod => $v_prod):
				//var_dump($v_prod->ID); echo '<hr />';
				$this->data['products']->$k_prod = $v_prod;
				$bidders =  $this->ion_product->get_users_bid($v_prod->ID);

				if(!empty($bidders))
					$this->data['products']->$k_prod->bidders = $bidders;

				//array_push($this->data['products']->$k_prod,);
				//$this->data['products']['info_bids'][$k_prod] = $v_prod;
			endforeach;
		endif;*/
		
		$this->data['topnav'] = $this->load->view('auth/blocks/top-nav', $this->data, TRUE);
		$this->data['leftside_nav'] = $this->load->view('auth/blocks/leftside-nav', $this->data, TRUE);
		$this->_render_page('auth/user_items', $this->data);
				
	}

	/*
	 * Import Items CSV format. 
	 */
	public function user_item_import() {

		if (!$this->ion_auth->logged_in())
				redirect('auth', 'refresh');


		$this->data['title'] = "Import Items";
		$this->data['nav_menu_left'][1]['active'] = 'active';
		$this->data['csv_data'] = array();
		$this->data['message'] = '';
		
		//validate form input
		$this->form_validation->set_rules('file_import', 'File Import:', 'required');
		

			//Data fields
			$this->data['file_import'] = array(
				'name'  => 'file_import',
				'type'  => 'file',
				'class' => 'form-control'
			);


		if(isset($_FILES['file_import'])):
			if($_FILES['file_import']['size'] <= 0)
				$this->error['file_size_error'] = 'Empty File Uploaded';
			if($_FILES['file_import']['size'] >	 3072000)
				$this->error['file_size_error'] = 'File limit Exceed. Upload a file lower that 3MB';
			if(pathinfo($_FILES['file_import']['name'], PATHINFO_EXTENSION) != 'csv')
				$this->error['file_type_error'] = 'Invalid File Uploaded';
		endif;
    	
    if(isset($_FILES['file_import']) AND empty($this->error)):

    	$file_import = $_FILES['file_import']['tmp_name']; 
		$file_handle = fopen($file_import,"r"); 

		//var_dump($file_import);
    	do { 
    		static $csv_counter = 0; 

        		$csv_data['name'] 		= isset($file_data[0]) ? $file_data[0] : '';
        		$csv_data['owner_id'] 	= $this->session->userdata('user_id');
        		$csv_data['s_description'] 	= isset($file_data[1]) ? $file_data[1] : '';
        		$csv_data['description'] 	= isset($file_data[2]) ? $file_data[2] : '';
        		$csv_data['price'] 		= isset($file_data[3]) ? $file_data[3] : '';
        		$csv_data['image_url'] 	= isset($file_data[4]) ? $file_data[4] : '';
        		$csv_data['bid_start'] 	= isset($file_data[5]) ? $file_data[5] : '';
        		$csv_data['bid_end'] 	= isset($file_data[6]) ? $file_data[6] : '';

		if(!empty($file_data)):
			if($csv_counter > 0):
				$this->data['csv_data'][] = $this->ion_product->insert_product_csv($csv_data);
    		endif;
    		$csv_counter++;
    	endif;

    	} while ($file_data = fgetcsv($file_handle,5000,";", "\"")); 

    	$this->data['message'] = 'Data has been succesfully added';
    else:
    	if(!empty($this->error))
    		$this->data['message'] = $this->error;
    endif;	

    	
    	$this->data['top_nav'] = $this->load->view('auth/blocks/top-nav', $this->data, TRUE);
		$this->data['leftside_nav'] = $this->load->view('auth/blocks/leftside-nav', $this->data, TRUE);
		$this->_render_page('auth/user_item_import', $this->data);


		#file_import
	}

	/*
	 * Add New Item
	 */
	public function user_item_add(){

		if (!$this->ion_auth->logged_in())
				redirect('auth', 'refresh');

		$this->data['title'] = "Add Collection Item";
		$this->data['nav_menu_left'][1]['active'] = 'active';

		//validate form input
		$this->form_validation->set_rules('product_name', 'Product Name:', 'required');
		$this->form_validation->set_rules('product_price', 'Product Price:', 'required');
		$this->form_validation->set_rules('product_image', 'Product Image:', 'required');
		$this->form_validation->set_rules('product_auction_start', 'Auction Start:', 'required');
		$this->form_validation->set_rules('product_auction_end', 'Auction End', 'required');
		$this->form_validation->set_rules('product_full_description', 'Product Description:', 'required');
		

			//Products Category
			$this->data['categories'] = $this->ion_product->get_product_categories();
			//Data fields
			$this->data['product_name'] = array(
				'name'  => 'product_name',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('product_name'),
			);
			$this->data['product_price'] = array(
				'name'  => 'product_price',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('product_price'),
			);
			$this->data['product_image'] = array(
				'name'  => 'product_image',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('product_image'),
			);
			$this->data['product_short_description'] = array(
				'name'  => 'product_short_description',
				'type'  => 'textarea',
				'rows'  => '3',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('product_short_description'),
			);
			$this->data['product_full_description'] = array(
				'name'  => 'product_full_description',
				'type'  => 'textarea',
				'rows'  => '8',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('product_full_description'),
			);

		//Validation Starts here;
		if ($this->form_validation->run() == true):
			#if(!is_int($this->session->userdata('user_id')) AND is_int($this->session->userdata('user_id')) > 0)
			#return false;

		//Process Save Items to Database
		$product_auction_start = date('Y-m-d H:i:s', strtotime($this->input->post('product_auction_start')));
		$product_auction_end   = date('Y-m-d H:i:s', strtotime($this->input->post('product_auction_end')));
		$data = array('owner_id'=> 	$this->session->userdata('user_id'),
					  'name'=> 	$this->input->post('product_name'),
				      'price'=> $this->input->post('product_price'),
				      'image_url'=>	$this->input->post('product_image'),
				      's_description'=> $this->input->post('product_short_description'),
				      'description'=> $this->input->post('product_full_description'),
				      'bid_start'=> $product_auction_start,
				      'bid_end'=>   $product_auction_end,
				      'category'=> $this->input->post('product_category') );

			//Record on the database
			$saved = $this->ion_product->save_product($data);
			if($saved == true):
				
				$this->data['message'] = '<div class="alert alert-success" role="alert"><p> Successfully Added</p></div>';
				
				//Reset value to empty if successfull
				foreach(array('product_name',
							  'product_price',
							  'product_image',
							  'product_short_description',
							  'product_full_description',
							  'product_auction_start',
							  'product_auction_end') as $d_reset):

					$this->data[$d_reset]['value'] = "";
				endforeach;

			else:
				if(!empty($saved)):
				$this->data['message'] = '';	
				foreach($saved as $k_edb => $v_edb):
					$data['message'] .= '<div class="alert alert-danger" role="alert"><p>'.$v_edb.'</p></div>';
				endforeach;
				endif;
			endif;
				
		else:
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$this->data['message'] = !empty($this->data['message']) ? '<div class="alert alert-danger" role="alert">'.$this->data['message'].'</div>' : '' ;	
			
		endif;	

		$this->data['leftside_nav'] = $this->load->view('auth/blocks/leftside-nav', $this->data, TRUE);
		$this->_render_page('auth/user_item_add', $this->data);
	}


	/*
	 * Product Detail
	 */
	public function product_detail($id, $params=array()) 
	{	
		$this->data['nav_menu_left'][0]['active'] = 'active';

		if (!$this->ion_auth->logged_in())
			redirect('auth/login/'.$id, 'refresh');
		else if(!$id)
			redirect('auth/index', 'refresh');

		//query product detail
		$this->data['product'] = $this->ion_product->get_product_detail($id);
		$this->data['product_bidders'] = $this->ion_product->get_users_bid($id);
		$this->data['product_user_check'] = $this->ion_product->search_data('user_id', $this->session->userdata('user_id'), $this->data['product_bidders']);

		if(!$this->data['product'])
			redirect('auth/index', 'refresh');

		//bid form fields validation
		$this->form_validation->set_rules('bid_amount', 'Bid Amount', 'required|numeric');

		if($this->form_validation->run() == true):
			/*
			 * Filter Hackers heres, bid price must have a higher value than current bid, server end filter
			 */
			
			if( $this->input->post('minimum_bid', TRUE) > $this->input->post('bid_amount', TRUE) ) {
				echo '<p>You have entered value lower than minimum bid!</p>';
				return;
			}

			//record bid product_id, user_id, bid price
			$this->ion_product->insert_user_bid($id, $this->session->userdata('user_id'), $this->input->post('bid_amount', TRUE));
			echo 'true'; 
			return;
		elseif(validation_errors()):
			//return errors
			echo validation_errors();
			return;
		endif;
		
		#var_dump(validation_errors());
		/*if($this->form_validation->run()) :
			echo validation_errors();
			return
		endif;*/
			
		$this->data['top_nav'] = $this->load->view('auth/blocks/top-nav', $this->data, TRUE);
		$this->data['leftside_nav'] = $this->load->view('auth/blocks/leftside-nav', $this->data, TRUE);
		$this->_render_page('auth/product-detail', $this->data);
	}

	//log the user in
	function login($id=false)
	{
		$this->data['title'] = "Login";

		//product id reference
		$this->data['id'] = $id;

		if ($this->ion_auth->logged_in()):
			if($id)
				return redirect('auth/product_detail/'.$id, 'refresh'); //Redirect To Detail Product Info
			return redirect('auth/index', 'refresh');
		endif;

		//validate form input
		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page

				$this->session->set_flashdata('message', $this->ion_auth->messages());
				if($id)
					redirect('auth/product_detail/'.$id, 'refresh'); //Redirect To Detail Product Info
				redirect('/', 'refresh');
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'class'=> 'form-control',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'class'=> 'form-control'
			);

			$this->_render_page('auth/login', $this->data);
		}
	}

	//log the user out
	function logout()
	{
		$this->data['title'] = "Logout";

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('product', 'refresh');
	}

	//change password
	function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->_render_page('auth/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	//forgot password
	function forgot_password()
	{
		//setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') == 'username' )
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_username_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('auth/forgot_password', $this->data);
		}
		else
		{
			// get identity from username or email
			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$identity = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
			}
			else
			{
				$identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
			}
	            	if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') == 'username')
		            	{
                                   $this->ion_auth->set_message('forgot_password_username_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_message('forgot_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->messages());
                		redirect("auth/forgot_password", 'refresh');
            		}

			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
				$this->_render_page('auth/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}


	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	//deactivate the user
	function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this pagexx.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth/deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			//redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	//create a new user
	function create_user()
	{
		$this->data['title'] = "Create User";

		if(!$this->config->item('membership', 'ion_auth')):
			if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
				redirect('auth', 'refresh');
		endif;
			

		$tables = $this->config->item('tables','ion_auth');

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique['.$tables['users'].'.email]');
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'required');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'company'    => $this->input->post('company'),
				'phone'      => $this->input->post('phone'),
			);
		}
		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data))
		{
			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['company'] = array(
				'name'  => 'company',
				'id'    => 'company',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('company'),
			);
			$this->data['phone'] = array(
				'name'  => 'phone',
				'id'    => 'phone',
				'type'  => 'text',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('phone'),
			);
			$this->data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'class' => 'form-control',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			$this->_render_page('auth/create_user', $this->data);
		}
	}

	//edit a user
	function edit_user($id)
	{
		$this->data['title'] = "Edit User";

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			//update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone'),
				);

				//update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			//check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		//display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->_render_page('auth/edit_user', $this->data);
	}

	// create a new group
	function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth/create_group', $this->data);
		}
	}

	//edit a group
	function edit_group($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		//validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'  => 'group_name',
			'id'    => 'group_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth/edit_group', $this->data);
	}


	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $render=false)
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}


	/*
	 *Joseph's Additionals functions
	 */
	public function register()
	{
		#Global Registration
		$this->create_user();
	}

}
