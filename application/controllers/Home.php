<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	private $template = 'default/';

	public function __construct(){
		parent::__construct();
		$this->load->Model('Downloads');
	}
	public function index()
	{
		$this->load->view( $this->template . 'index');
	}

	public function download()
	{
		$check = $this->Downloads->check_download('213sdjn67da');
		if(!$check)
			return false;

		$file_download = FCPATH.'downloads\\'.$check->row()->name;

		#var_dump();
		#var_dump($file_download);
		#var_dump($check->result_array());
		header('Content-Type: application/download');
    	header('Content-Disposition: attachment; filename="'.$check->row()->name.'"');
    	header("Content-Length: " . filesize($file_download));

    	$fp = fopen($file_download, "r");
    	fpassthru($fp);
    	fclose($fp);
	}
}
